FROM maven:3-eclipse-temurin-17-focal
WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

COPY src src
RUN mvn clean package

EXPOSE 8080
CMD ["mvn", "exec:java"]

