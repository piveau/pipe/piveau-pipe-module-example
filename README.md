# piveau Pipe Module Example

This is basic example for a custom piveau pipe module

## Instructions

Build it 
```
$ docker build -t piveau-pipe-module-example .
```
Run it: 

```
$ docker compose up -d
```


## Authors
Fabian Kirstein, Simon Dutkowski