package io.piveau.pipe.module;

import io.piveau.pipe.connector.PipeConnector;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Launcher;
import io.vertx.core.Promise;
import io.piveau.rdf.Piveau;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCAT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;

/**
 * This is a showcase Vert.x web service to use in tutorials.
 */
public class MainVerticle extends AbstractVerticle {

    // This is a Logger - always use a Logger!
    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * The main Vert.x function aka entry point to the application
     *
     * @param startPromise
     * @throws Exception
     */
    @Override
    public void start(Promise<Void> startPromise) throws Exception {

        Future<PipeConnector> pipeConnector = PipeConnector.create(vertx);
        pipeConnector.onSuccess(connector -> {
            connector.handlePipe(pipeContext -> {

                Model model = Piveau.toModel(
                        pipeContext.getStringData().getBytes(),
                        Lang.NTRIPLES
                );

                Resource dataset = Piveau.findDatasetAsResource(model);

                if(dataset != null) {
                    dataset.addProperty(DCAT.keyword, "fish");
                }

                pipeContext.setResult(
                        Piveau.presentAs(model, Lang.NTRIPLES),
                        Lang.NTRIPLES.getHeaderString(),
                        pipeContext.getDataInfo())
                        .forward();
            });
        });

        startPromise.complete();
    }

    /**
     * This is an optional function which is handy if you want to start your app form an IDE.
     *
     * @param args
     */
    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }
}
